#include "Game.h"
#include "BackBuffer.h"
#include "Render.h"

CGame::CGame()
{
	gravity = new b2Vec2(0.0f, -10.f);
	world = new b2World(*gravity);

	/// GroundBody
	b2BodyDef groundBodyDef;
	groundBodyDef.position.Set(0.0f, -10.0f);
	b2Body* groundBody = world->CreateBody(&groundBodyDef);
	b2PolygonShape groundBox;
	groundBox.SetAsBox(50.f, 10.f);
	groundBody->CreateFixture(&groundBox, 0.0f);

	/// Body
	b2BodyDef bodyDef;	
	bodyDef.type = b2_dynamicBody;	// only dynamicBody is influenced by forces
	bodyDef.position.Set(0.f, 4.f);
	body = world->CreateBody(&bodyDef);
	b2PolygonShape dynamicBox;	
	dynamicBox.SetAsBox(1.f,1.f);

	/// Fixture
	b2FixtureDef fixtureDef;
	fixtureDef.shape = &dynamicBox;
	fixtureDef.density = 1.f;
	fixtureDef.friction = .3f;
	body->CreateFixture(&fixtureDef);

	fTimeStep = 1.f/60.f;
	iVelocityIterations = 6;
	iPositionIterations = 2;

	backBuffer = new CBackBuffer();
}

bool CGame::Initialize(HINSTANCE _hInstance, HWND _hWnd, int _iWidth, int _iHeight, CTimer& timer)
{
	// Hold onto drawing objects
	mhMainInstance = _hInstance;
	mhMainWindow = _hWnd;
	miWidth = _iWidth;
	miHeight = _iHeight;

	backBuffer->Initialise(_hWnd, _iWidth, _iHeight);

	mp_Render = CRender::GetInstance();
	mp_Render->mp_BackBuffer = backBuffer;

	world->SetDebugDraw(mp_Render);

	

	EnforceWindowSize(_hWnd);
		
	mpTimer = &timer;
	assert(mpTimer);
		
	return true;
}

void CGame::Run(float _fTimeStep)
{
	fTimeStep = _fTimeStep;
}

void CGame::Draw()
{
	backBuffer->Clear();
	world->Step(fTimeStep, iVelocityIterations, iPositionIterations);

	b2Vec2 position = body->GetPosition();
	
	world->DrawDebugData();

	float32 angle = body->GetAngle();

	SelectObject(backBuffer->GetBackDC(), GetStockObject(WHITE_PEN));

	Ellipse ( backBuffer->GetBackDC(), 10, 20, 30, 40 );
	
	backBuffer->Present();
}

CGame::~CGame(void)
{
}

// Ensure the window is the same size as the back buffer
void CGame::EnforceWindowSize(HWND hWnd)
{
	// here, we want to ensure the window matches the backbuffer size
	if ( IsWindow( hWnd ) )
	{
		DWORD dwStyle = GetWindowLongPtr( hWnd, GWL_STYLE ) ;
		DWORD dwExStyle = GetWindowLongPtr( hWnd, GWL_EXSTYLE ) ;
		HMENU menu = GetMenu( hWnd ) ;

		RECT rc = { 0, 0, backBuffer->m_iWidth, backBuffer->m_iHeight } ;

		AdjustWindowRectEx( &rc, dwStyle, menu ? TRUE : FALSE, dwExStyle );

		SetWindowPos( hWnd, NULL, 0, 0, rc.right - rc.left, rc.bottom - rc.top, SWP_NOZORDER | SWP_NOMOVE ) ;
	}
}