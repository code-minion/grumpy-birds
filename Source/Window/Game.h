#pragma once
#include <Box2D\Box2D.h>
#include "Timer.h"
#include "BackBuffer.h"
#include "Render.h"
#include <Windows.h>

class CGame
{
public:
	/// Run-once functions
	CGame();
	~CGame(void);
	bool Initialize(HINSTANCE,HWND,int w,int h, CTimer&);
	void EnforceWindowSize (HWND hWnd);

	/// Loop Functions
	void Run(float msTimeStep);		// Calls other processes in order	
	void Input();					// Process Inputs since last delta
	void Logic(float msTimeStep);	// Process Game Logic since last delta, Includes Physics
	void Draw();					// Draw current drame
	
	/// Utility functions

private:
	/// Window Drawing Objects/Variables
	int miWidth;
	int miHeight;
	HINSTANCE mhMainInstance;
	HWND mhMainWindow;
	HDC mhDC;
	HBITMAP mhSurface;
	HBITMAP mhOldObject;
	CBackBuffer* backBuffer;

	/// Timer
	CTimer* mpTimer;

	/// Game Logic Variables
	float fTimeStep;
	int iVelocityIterations;
	int iPositionIterations;
	
	/// Box2D World Factory
	b2World* world;

	/// Box2D Static variables
	b2Vec2* gravity;

	/// Box2D Dynamic Variables
	b2Body* body;

	CRender* mp_Render;
};

