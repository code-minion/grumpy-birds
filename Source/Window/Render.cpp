#include "Render.h"

CRender* CRender::mp_Render = new CRender;


using namespace std;

CRender::CRender()
{
	b2Draw::SetFlags(b2Draw::e_shapeBit);
}

CRender* CRender::GetInstance()
{
	if (!mp_Render)
	{
		mp_Render = new CRender();
	}

	return (mp_Render);
}


void CRender::DestroyInstance()
{
	delete mp_Render;
	mp_Render = 0;
}

void CRender::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdicolor = CreatePen(PS_SOLID,1, RGB(color.r,color.g,color.b));	
	SelectObject(dc, gdicolor);
	SelectObject(dc, GetStockObject(HOLLOW_BRUSH));
	POINT verts[256];

	if (vertexCount > 256){
		vertexCount = 256;
	}

	for (int32 i = 0; i < vertexCount; ++i)
	{
		verts[i].x = vertices[i].x;
		verts[i].y = vertices[i].y;
	}

	Polygon(mp_BackBuffer->GetBackDC(), verts, vertexCount);	
	DeleteObject(gdicolor);
}

void CRender::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdipen = CreatePen(PS_SOLID,1, RGB(color.r,color.g,color.b));	
	HBRUSH gdibrush = CreateSolidBrush(RGB(color.r, color.g, color.b));
	SelectObject(dc, gdipen);
	SelectObject(dc, gdibrush);

	if (vertexCount > 256){
		vertexCount = 256;
	}

	POINT verts[256];
	for (int32 i = 0; i < vertexCount; ++i)
	{
		verts[i].x = vertices[i].x + 300;
		verts[i].y = vertices[i].y + 300;
	}

	Polygon(mp_BackBuffer->GetBackDC(), verts, vertexCount);//&reinterpret_cast<const POINT*>(vertices)[0], vertexCount);	
	DeleteObject(gdibrush);	
	DeleteObject(gdipen);
}

void CRender::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdicolor = CreatePen(PS_SOLID,1, RGB(color.r,color.g,color.b));	
	SelectObject(dc, gdicolor);
	SelectObject(dc, GetStockObject(HOLLOW_BRUSH));

	Ellipse(dc, center.x - radius, center.y - radius, center.x + radius, center.y + radius);

	DeleteObject(gdicolor);
}

void CRender::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdipen = CreatePen(PS_SOLID,1, RGB(color.r,color.g,color.b));	
	HBRUSH gdibrush = CreateSolidBrush(RGB(color.r, color.g, color.b));
	SelectObject(dc, gdipen);
	SelectObject(dc, gdibrush);
	
	Ellipse(dc, center.x - radius, center.y - radius, center.x + radius, center.y + radius);

	DeleteObject(gdipen);
	DeleteObject(gdibrush);
}

void CRender::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdicolor = CreatePen(PS_SOLID,1, RGB(color.r,color.g,color.b));	
	SelectObject(dc, gdicolor);
	SelectObject(dc, GetStockObject(HOLLOW_BRUSH));

	Rectangle(dc , p1.x, p1.y, p2.x, p2.y);

	DeleteObject(gdicolor);
}

void CRender::DrawTransform(const b2Transform& xf)
{
	b2Vec2 p1 = xf.p, p2;
	const float32 k_axisScale = 0.4f;
	
	p2 = p1 + k_axisScale * xf.q.GetXAxis();
	DrawSegment(p1,p2,b2Color(1,0,0));

	p2 = p1 + k_axisScale * xf.q.GetYAxis();
	DrawSegment(p1,p2,b2Color(0,1,0));
}

void CRender::DrawPoint(const b2Vec2& p, float32 size, const b2Color& color)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdipen = CreatePen(PS_SOLID,1, RGB(color.r,color.g,color.b));	
	HBRUSH gdibrush = CreateSolidBrush(RGB(color.r, color.g, color.b));
	SelectObject(dc, gdipen);
	SelectObject(dc, gdibrush);
	
	Ellipse(dc, p.x - size, p.y - size, p.x + size, p.y + size);

	DeleteObject(gdipen);
	DeleteObject(gdibrush);
}

void CRender::DrawString(int x, int y, const char *string, ...)
{
	char buffer[128];
	RECT r = {10,10,20,20};

	HDC dc = mp_BackBuffer->GetBackDC();
	SelectObject(dc, GetStockObject(BLACK_PEN));
	DrawTextA(dc, string, strlen(string), &r, RGB(12,12,12));
}

void CRender::DrawAABB(b2AABB* aabb, const b2Color& c)
{
	HDC dc = mp_BackBuffer->GetBackDC();
	HPEN gdicolor = CreatePen(PS_SOLID,1, RGB(c.r,c.g,c.b));	
	SelectObject(dc, gdicolor);
	SelectObject(dc, GetStockObject(HOLLOW_BRUSH));

	Rectangle(dc , aabb->lowerBound.x, aabb->lowerBound.y, aabb->upperBound.x, aabb->upperBound.y);

	DeleteObject(gdicolor);
}