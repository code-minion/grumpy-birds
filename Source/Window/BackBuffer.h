#pragma once
#include <Windows.h>

// minimal back buffer
class CBackBuffer
{
public:
	CBackBuffer(void);
	~CBackBuffer(void);

	bool Initialise(HWND _hWnd, int _iWidth, int _iHeight);
	void Clear(void);
	void Present(void);
	HDC GetBackDC(void);

	HWND	m_hWnd;
	HDC		m_hDC;
	HBITMAP m_hSurface;
	HBITMAP m_hOldObject;
	int		m_iHeight;
	int		m_iWidth;
};
