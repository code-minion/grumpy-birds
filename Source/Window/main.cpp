
#define WIN32_LEAN_AND_MEAN
#define WINDOW_CLASS_NAME L"GRUMPYBIRDS"

/// Library Includes
#include <windows.h>
#include "Game.h"

/// Static Variables
HINSTANCE ghInstance;	// main window instance

/// Globals
HDC bitmap_hdc;
HBITMAP handle_to_bitmap;
CTimer Timer;
CGame* Game;
float gfTimeAccumulator;
int giClientWidth;
int giClientHeight;

LRESULT CALLBACK WindowProc(HWND _hWnd, UINT _uiMsg, WPARAM _wParam, LPARAM _lParam);
HWND CreateAndRegisterWindow(HINSTANCE _hInstance, int _iWidth, int _iHeight, const char* _pcTitle);

int WINAPI WinMain(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPSTR _lpCmdline, int _iCmdshow)
{
	// handle warning for ignored params
	_hPrevInstance;

	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	ghInstance = _hInstance;

	HWND hwnd =	CreateAndRegisterWindow(_hInstance, 800, 600, "Grumpy Birds");
	if(!hwnd)
	{
		return(0);
	}

	Timer.reset();

	// Init Game
	Game = new CGame();
	Game->Initialize(_hInstance, hwnd, 800,600, Timer);

	while(msg.message != WM_QUIT)
	{
		if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE))	{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			Timer.tick();
			//Timer.getDeltaTime();
			Game->Draw();
		}
	}

	return 0;
}


LRESULT CALLBACK
WindowProc(HWND _hWnd, UINT _uiMsg, WPARAM _wParam, LPARAM _lParam)
{
	switch (_uiMsg)
	{
	case WM_LBUTTONUP:
		{
		}
		break;

	case WM_KEYUP:
		{
		}
		break;

	case WM_PAINT:
		{
			HDC var = BeginPaint(_hWnd, 0);

			Ellipse(var, 0, 0, 10, 10);

			EndPaint(_hWnd, 0);
		}
		break;
		
	case WM_DESTROY:
		{
			PostQuitMessage(0);
		}
		break;

	default:break;
	}

	return (DefWindowProc(_hWnd, _uiMsg, _wParam, _lParam));
}

HWND
CreateAndRegisterWindow(HINSTANCE _hInstance, int _iWidth, int _iHeight, const char* _pcTitle)
{
	WNDCLASSEX winclass;
	ZeroMemory(&winclass, sizeof(WNDCLASSEX));

	winclass.cbSize = sizeof(WNDCLASSEX);
	winclass.style = CS_HREDRAW | CS_VREDRAW;
	winclass.lpfnWndProc = WindowProc;
	winclass.cbClsExtra = 0;
	winclass.cbWndExtra = 0;
	winclass.hInstance = _hInstance;
	winclass.hIcon = 0;
	winclass.hCursor = 0;
	winclass.hbrBackground = static_cast<HBRUSH>(GetStockObject(BLACK_BRUSH));
	winclass.lpszMenuName = NULL;
	winclass.lpszClassName = WINDOW_CLASS_NAME;
	winclass.hIconSm = 0;

	if (!RegisterClassEx (&winclass))
	{
		// Failed to register.
		MessageBox(0, L"fail", L"fail", 0);

		return (0);
	}

	HWND hwnd;
	hwnd = CreateWindowEx(NULL, 
						  WINDOW_CLASS_NAME,
						  L"Grumpy Birds",
						  WS_BORDER | WS_CAPTION | WS_SYSMENU | WS_VISIBLE,
						  CW_USEDEFAULT, CW_USEDEFAULT,
						  _iWidth, _iHeight,
						  NULL,
						  NULL,
						  _hInstance,
						  0);

	if (!hwnd)
	{
		// Failed to create.


		return (0);
	}

	return (hwnd);
}