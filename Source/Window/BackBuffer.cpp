
#include <windows.h>
#include <WinGDI.h>
#include "BackBuffer.h"

// Default Constructor
CBackBuffer::CBackBuffer(void)
{
}

// Default Destructor
CBackBuffer::~CBackBuffer(void)
{
	SelectObject(m_hDC, m_hOldObject);

	DeleteObject(m_hSurface);
	DeleteObject(m_hDC);
}


//
// Initialises the backbuffer.
//
// @param	_hWnd			Handle to the window.
// @param	_iWidth			Width of the window.
// @param	_iHeight		Height of the window.
// @return	Returns true if successfull.
//
bool CBackBuffer::Initialise(HWND _hWnd, int _iWidth, int _iHeight)

{
	m_hWnd = _hWnd;
	m_iWidth = _iWidth;
	m_iHeight = _iHeight;

	HDC hWindowDC = ::GetDC(m_hWnd);
	m_hDC = CreateCompatibleDC(hWindowDC);
	m_hSurface = CreateCompatibleBitmap(hWindowDC, m_iWidth, m_iHeight);\
	ReleaseDC(m_hWnd, hWindowDC);

	m_hOldObject = static_cast<HBITMAP>(SelectObject(m_hDC, m_hSurface));

	HBRUSH brushWhite = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));

	HBRUSH oldBrush = static_cast<HBRUSH>(SelectObject(m_hDC, brushWhite));
	Rectangle(m_hDC, 0, 0, m_iWidth, m_iHeight);
	SelectObject(m_hDC, oldBrush);

	return (true);

}


// Clears the backbuffer.
void CBackBuffer::Clear(void)
{
	HBRUSH oldBrush = static_cast<HBRUSH>(SelectObject(m_hDC, GetStockObject(WHITE_BRUSH)));
	HPEN oldPen = static_cast<HPEN>(SelectObject(m_hDC, GetStockObject(WHITE_PEN)));
	Rectangle(m_hDC, 0, 0, m_iWidth, m_iHeight);
	SelectObject(m_hDC, oldPen);
	SelectObject(m_hDC, oldBrush);
}


// Presents the backbuffer to the screen.
void CBackBuffer::Present(void)
{
	HDC hWndDC = ::GetDC(m_hWnd);
	BitBlt(hWndDC, 0, 0, m_iWidth, m_iHeight, m_hDC, 0, 0, SRCCOPY);
	ReleaseDC(m_hWnd, hWndDC);
}

// Get the back buffers device context.
//
// @return	Returns the handle to the backbuffers DC.
//
HDC CBackBuffer::GetBackDC(void)
{
	return (m_hDC);
}
