//////////////////////////////////////////////////////////
//
//	Render Tools modified from Box2D Testbed by Erin Catto
//
//////////////////////////////////////////////////////////

#ifndef RENDER_H
#define RENDER_H

#include <Box2D/Box2D.h>
#include "BackBuffer.h"
#include <Windows.h>

struct b2AABB;

// This class implements debug drawing callbacks that are invoked
// inside b2World::Step.
class CRender : public b2Draw
{
public:
	CRender();

	static CRender* GetInstance();

	void DestroyInstance();

	virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

	virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color);

	virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color);

	virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color);

	virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color);

	virtual void DrawTransform(const b2Transform& xf);

    virtual void DrawPoint(const b2Vec2& p, float32 size, const b2Color& color);

    virtual void DrawString(int x, int y, const char* string, ...); 

    virtual void DrawAABB(b2AABB* aabb, const b2Color& color);

	
	CBackBuffer*		mp_BackBuffer;

private:

	static CRender*		mp_Render;
};


#endif
